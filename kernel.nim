import limine

# TODO: Nim can't compile array assignment without memcpy, so see if there's a way to trick the compiler into doing it.
const tr = limine.LIMINE_TERMINAL_REQUEST

# Limine requests need to be declared with the volatile pragma to ensure the underlying C compiler
# doesn't optimize them away.
let terminal_request {.volatile.} = limine.limine_terminal_request(
  id: [tr[0], tr[1], tr[2], tr[3]],
  revision: 0'u
)

proc done(): void {.noReturn.} =
  while true:
    asm """hlt"""

# This is our kernel's entry point. We can call it whatever we want (start, main, kmain, etc.),
# as long as we export it with the name "_start".
proc start(): uint64 {.exportc: "_start" .} =
  # Ensure we got a terminal.
  if terminal_request.response == 0'u64:
    done()

  let terminal_response = cast[ptr limine.limine_terminal_response](terminal_request.response)
  if terminal_response.terminal_count < 1:
    done()

  # We should now be able to call the Limine terminal to print out a simple "Hello World" to the screen.
  let terminal: ptr limine.limine_terminal = (cast [UncheckedArray[ptr limine_terminal]] (terminal_response.terminals))[0]
  let write = cast[limine.limine_terminal_write](terminal_response.write)
  write(terminal, cstring("Hello world"), 11);

  # We're done, so just hang...
  done()
