# Converted from limine.h using c2nim and the following options:
# #def LIMINE_PTR(TYPE) uint64_t
# #assumendef LIMINE_NO_POINTERS
# #assumendef _LIMINE_H
# #assumendef __cplusplus
# #assumedef __x86_64__
#
# Other translation notes:
# The LIMINE_COMMON_MAGIC #define was broken into 2 for this file.
# c2nim by default generates 32-bit code, but Limine is 64-bit only, so all "'u" literals have been manually changed to "'u64"

const
  LIMINE_COMMON_MAGIC_1* =
    0xc7b1dd30df4c8b88'u64
  LIMINE_COMMON_MAGIC_2* =
    0x0a82e883a194f07b'u64

type
  limine_uuid* {.bycopy.} = object
    a*: uint32
    b*: uint16
    c*: uint16
    d*: array[8, uint8]


const
  LIMINE_MEDIA_TYPE_GENERIC* = 0
  LIMINE_MEDIA_TYPE_OPTICAL* = 1
  LIMINE_MEDIA_TYPE_TFTP* = 2

type
  limine_file* {.bycopy.} = object
    revision*: uint64
    address*: uint64
    size*: uint64
    path*: uint64
    cmdline*: uint64
    media_type*: uint32
    unused*: uint32
    tftp_ip*: uint32
    tftp_port*: uint32
    partition_index*: uint32
    mbr_disk_id*: uint32
    gpt_disk_uuid*: limine_uuid
    gpt_part_uuid*: limine_uuid
    part_uuid*: limine_uuid



const
  LIMINE_BOOTLOADER_INFO_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0xf55038d8e2a1202f'u64,
    0x279426fcf5f59740'u64]

type
  limine_bootloader_info_response* {.bycopy.} = object
    revision*: uint64
    name*: uint64
    version*: uint64

  limine_bootloader_info_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_STACK_SIZE_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x224ef0460a8e8926'u64,
    0xe1cb0fc25f46ea3d'u64]

type
  limine_stack_size_response* {.bycopy.} = object
    revision*: uint64

  limine_stack_size_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64
    stack_size*: uint64



const
  LIMINE_HHDM_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x48dcf1cb8ad2b852'u64,
    0x63984e959a98244b'u64]

type
  limine_hhdm_response* {.bycopy.} = object
    revision*: uint64
    offset*: uint64

  limine_hhdm_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_FRAMEBUFFER_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x9d5827dcd881dd75'u64,
    0xa3148604f6fab11b'u64]
  LIMINE_FRAMEBUFFER_RGB* = 1

type
  limine_video_mode* {.bycopy.} = object
    pitch*: uint64
    width*: uint64
    height*: uint64
    bpp*: uint16
    memory_model*: uint8
    red_mask_size*: uint8
    red_mask_shift*: uint8
    green_mask_size*: uint8
    green_mask_shift*: uint8
    blue_mask_size*: uint8
    blue_mask_shift*: uint8

  limine_framebuffer* {.bycopy.} = object
    address*: uint64
    width*: uint64
    height*: uint64
    pitch*: uint64
    bpp*: uint16
    memory_model*: uint8
    red_mask_size*: uint8
    red_mask_shift*: uint8
    green_mask_size*: uint8
    green_mask_shift*: uint8
    blue_mask_size*: uint8
    blue_mask_shift*: uint8
    unused*: array[7, uint8]
    edid_size*: uint64
    edid*: uint64
    mode_count*: uint64
    modes*: uint64

  limine_framebuffer_response* {.bycopy.} = object
    revision*: uint64
    framebuffer_count*: uint64
    framebuffers*: uint64

  limine_framebuffer_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_TERMINAL_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0xc8ac59310c2b0844'u64,
    0xa68d0c7265d38878'u64]
  LIMINE_TERMINAL_CB_DEC* = 10
  LIMINE_TERMINAL_CB_BELL* = 20
  LIMINE_TERMINAL_CB_PRIVATE_ID* = 30
  LIMINE_TERMINAL_CB_STATUS_REPORT* = 40
  LIMINE_TERMINAL_CB_POS_REPORT* = 50
  LIMINE_TERMINAL_CB_KBD_LEDS* = 60
  LIMINE_TERMINAL_CB_MODE* = 70
  LIMINE_TERMINAL_CB_LINUX* = 80
  LIMINE_TERMINAL_CTX_SIZE* = high(uint64)
  LIMINE_TERMINAL_CTX_SAVE* = high(uint64) - 1
  LIMINE_TERMINAL_CTX_RESTORE* = high(uint64) - 2
  LIMINE_TERMINAL_FULL_REFRESH* = high(uint64) - 3

discard "forward decl of limine_terminal"
type
  limine_terminal_write* = proc (a1: ptr limine_terminal; a2: cstring; a3: uint64) {.nimcall.}
  limine_terminal_callback* = proc (a1: ptr limine_terminal; a2: uint64; a3: uint64;
                                 a4: uint64; a5: uint64)
  limine_terminal* {.bycopy.} = object
    columns*: uint64
    rows*: uint64
    framebuffer*: uint64

  limine_terminal_response* {.bycopy.} = object
    revision*: uint64
    terminal_count*: uint64
    terminals*: uint64
    write*: uint64

  limine_terminal_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64
    callback*: uint64



const
  LIMINE_5_LEVEL_PAGING_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x94469551da9b3192'u64,
    0xebe5e86db7382888'u64]

type
  limine_5_level_paging_response* {.bycopy.} = object
    revision*: uint64

  limine_5_level_paging_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_SMP_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x95a67b819a1b857e'u64,
    0xa0b61b723b6a73e0'u64]

const
  LIMINE_SMP_X2APIC* = (1 shl 0)

type
  limine_smp_info* {.bycopy.} = object
    processor_id*: uint32
    lapic_id*: uint32
    reserved*: uint64
    goto_address*: uint64
    extra_argument*: uint64

  limine_smp_response* {.bycopy.} = object
    revision*: uint64
    flags*: uint32
    bsp_lapic_id*: uint32
    cpu_count*: uint64
    cpus*: uint64

  limine_smp_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64
    flags*: uint64

type
  limine_goto_address* = proc (a1: ptr limine_smp_info)

const
  LIMINE_MEMMAP_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x67cf3d9d378a806f'u64,
    0xe304acdfc50c3c62'u64]
  LIMINE_MEMMAP_USABLE* = 0
  LIMINE_MEMMAP_RESERVED* = 1
  LIMINE_MEMMAP_ACPI_RECLAIMABLE* = 2
  LIMINE_MEMMAP_ACPI_NVS* = 3
  LIMINE_MEMMAP_BAD_MEMORY* = 4
  LIMINE_MEMMAP_BOOTLOADER_RECLAIMABLE* = 5
  LIMINE_MEMMAP_KERNEL_AND_MODULES* = 6
  LIMINE_MEMMAP_FRAMEBUFFER* = 7

type
  limine_memmap_entry* {.bycopy.} = object
    base*: uint64
    length*: uint64
    `type`*: uint64

  limine_memmap_response* {.bycopy.} = object
    revision*: uint64
    entry_count*: uint64
    entries*: uint64

  limine_memmap_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_ENTRY_POINT_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x13d86c035a1cd3e1'u64,
    0x2b0caa89d8f3026a'u64]

type
  limine_entry_point* = proc ()
  limine_entry_point_response* {.bycopy.} = object
    revision*: uint64

  limine_entry_point_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64
    entry*: uint64



const
  LIMINE_KERNEL_FILE_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0xad97e90e83f1ed67'u64,
    0x31eb5d1c5ff23b69'u64]

type
  limine_kernel_file_response* {.bycopy.} = object
    revision*: uint64
    kernel_file*: uint64

  limine_kernel_file_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_MODULE_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x3e7e279702be32af'u64,
    0xca1c4f3bd1280cee'u64]

type
  limine_module_response* {.bycopy.} = object
    revision*: uint64
    module_count*: uint64
    modules*: uint64

  limine_module_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_RSDP_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0xc5e77b6b397e7b43'u64,
    0x27637845accdcf3c'u64]

type
  limine_rsdp_response* {.bycopy.} = object
    revision*: uint64
    address*: uint64

  limine_rsdp_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_SMBIOS_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x9e9046f11e095391'u64,
    0xaa4a520fefbde5ee'u64]

type
  limine_smbios_response* {.bycopy.} = object
    revision*: uint64
    entry_32*: uint64
    entry_64*: uint64

  limine_smbios_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_EFI_SYSTEM_TABLE_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x5ceba5163eaaf6d6'u64,
    0x0a6981610cf65fcc'u64]

type
  limine_efi_system_table_response* {.bycopy.} = object
    revision*: uint64
    address*: uint64

  limine_efi_system_table_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_BOOT_TIME_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x502746e184c088aa'u64,
    0xfbc5ec83e6327893'u64]

type
  limine_boot_time_response* {.bycopy.} = object
    revision*: uint64
    boot_time*: int64

  limine_boot_time_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_KERNEL_ADDRESS_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0x71ba76863cc55f63'u64,
    0xb2644a48c516a487'u64]

type
  limine_kernel_address_response* {.bycopy.} = object
    revision*: uint64
    physical_base*: uint64
    virtual_base*: uint64

  limine_kernel_address_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64



const
  LIMINE_DTB_REQUEST* = [LIMINE_COMMON_MAGIC_1, LIMINE_COMMON_MAGIC_2, 0xb40ddb48fb54bac7'u64,
    0x545081493f81ffb7'u64]

type
  limine_dtb_response* {.bycopy.} = object
    revision*: uint64
    dtb_ptr*: uint64

  limine_dtb_request* {.bycopy.} = object
    id*: array[4, uint64]
    revision*: uint64
    response*: uint64
