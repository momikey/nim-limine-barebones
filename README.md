# Limine Bare Bones in Nim

This is a port of the [Limine Bare Bones](https://github.com/limine-bootloader/limine-barebones) kernel to the [Nim](https://nim-lang.org) language.

## Changes from the original

I've made a couple of changes from the original repo, both to ease development and to fit my personal preferences:

* The default compiler is set to Clang instead of GCC. Since Clang is a cross-compiler by default, this is easier than building or installing a whole new GCC toolchain.
* There is no makefile. Larger OS projects will likely need one if they mix languages, but this project is intended to be pure Nim as much as possible.
* The resulting binary is called `kernel.elf`. `myos` just looks silly to me.

## Building

The ``build.sh` file is simply a copy of the "Creating an ISO" section of the [Bare Bones tutorial](https://wiki.osdev.org/Limine_Bare_Bones). These commands work with no change. You'll need git and xorriso installed, of course.

## Files

Because Nim is structured in a different way than C, the files in this repo don't match up perfectly with those in the original.

### kernel.nim

This is the main (in this project, the only) kernel module. It's a relatively straightforward port of the Bare Bones `kernel.c` file. I did have to work around the langauges' different parsing rules.

### kernel.nim.cfg

This holds the compiler options for `kernel.nim`. It sets the C compiler to Clang and the linker to LLD, so no need for a separate toolchain. The options passed to Clang and LLD are essentially the same as in the Bare Bones makefile. Everything else is Nim-specific: "standalone" OS, disabling the GC, and removing the generated `main` function.

### limine.cfg

Just a copy of the same file from the Bare Bones repo. No changes necessary.

### limine.nim

Holds definitions of types and constants. To build it, I ran `limine.h` through c2nim, defining macros as necessary to get decent output. Then, I changed a few things that c2nim just couldn't handle, like the way the Limine magic numbers are defined.

This is kind of a brute-force, "get it done" port. I'd like to do a more official Limine binding package in the future.

### linker.ld

The linker script, with no changes from Bare Bones.

### panicoverride.nim

When building standalone binaries in Nim, you have to provide your own definitions for the system procs `panic` and `rawoutput`. As this project is made to output through other means and to never panic, we don't need actual code for the procs. If you're building your own OS, however, you'll probably need to create custom panic routines.

# License

This project is open source software under the [MIT License](LICENSE).
